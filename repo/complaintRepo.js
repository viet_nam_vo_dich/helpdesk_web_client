var axios = require('axios');
var dateFormat = require('dateformat');

async function GetAll(user, filter) {
    var config = {
        headers: {
            'Content-type': 'application/json',
            'Authorization': user.auth,
        }
    }
    var result = await axios.get('http://dev.helpdesk.com/api/complaint/getall', config).then(function (response) {
        if (response.status == 200 || response.status == 204) {
            return response.data;
        } else {
            console.log(response);
            return undefined;
        }
    }).catch(function (error) {
        if (error.response.status == 403) {
            return 'Token expired';
        } else {
            console.log(error);
            return undefined;
        }
    });
    return result;
}

async function Get(user, id) {
    var config = {
        headers: {
            'Content-type': 'application/json',
            'Authorization': user.auth,
        }
    }
    var result = await axios.get('http://dev.helpdesk.com/node/' + id + '?_format=json', config).then(function (response) {
        if (response.status == 200) {
            var data = response.data;

            var technician = [];
            data.field_technician.forEach(function (val) {
                technician.push(val.target_id);
            });

            var complaint = {
                id: data.nid[0].value,
                subject: data.title[0].value,
                category: (data.field_category.length > 0 ? data.field_category[0].target_id : ''),
                content: data.field_content[0].value,
                deadline: (data.field_deadline.length > 0 ? dateFormat(data.field_deadline[0].value, 'yyyy-mm-dd"T"HH:MM') : ''),
                department: (data.field_department.length > 0 ? data.field_department[0].target_id : ''),
                priority: data.field_priority[0].value,
                rejectReason: (data.field_reject_reason.length > 0 ? data.field_reject_reason[0].value : ''),
                resolveTime: (data.field_resolve_time.length > 0 ? dateFormat(data.field_resolve_time[0].value, 'yyyy-mm-dd"T"HH:MM') : ''),
                status: data.field_status[0].target_id,
                technician: technician,
            };

            return complaint;
        } else {
            console.log(response);
            return undefined;
        }
    }).catch(function (error) {
        if (error.response.status == 403) {
            return 'Token expired';
        } else {
            console.log(error);
            return undefined;
        }
    });
    return result;
}

async function Update(user, id, complaint) {
    if(id == '0') {
        var url = 'http://dev.helpdesk.com/node?_format=json';
        var method = 'post';
    } else {
        var url = 'http://dev.helpdesk.com/node/'+id+'?_format=json';
        var method = 'patch';
    }

    var technician = [];
    if(Array.isArray(complaint.technician)){
        complaint.technician.forEach(function(val){
            if(val != 0) {
                technician.push({target_id: val});
            }
        });
    } else {
        if(complaint.technician != 0) {
            technician.push(complaint.technician);
        }
    }


    var data = {
        type: [{target_id: 'complaint'}],
        field_priority: [{value: complaint.priority}],
        field_category: (complaint.category != 0 ? [{target_id: complaint.category}] : []),
        title: [{value: complaint.subject}],
        field_content: [{value: complaint.content}],
        field_department: (complaint.department != 0 ? [{target_id: complaint.department}] : []),
        field_status: [{target_id: complaint.status}],
        field_deadline: (complaint.deadline != '' ? [{value: complaint.deadline+':00'}] : []),
        field_resolve_time: (complaint.resolve_time != '' ? [{value: complaint.resolve_time+':00'}] : []),
        field_reject_reason: (complaint.reject_reason != '' ? [{value: complaint.reject_reason}] : []),
        field_technician: technician,
    }

    var result = await axios({
        method: method,
        url: url,
        data: data,
        headers: {
            'Content-type': 'application/json',
            'Authorization': user.auth,
            'X-CSRF-Token': user.csrf,
        }
    }).then(function (response) {
        if (response.status == 200 || response.status == 201) {
            return true;
        } else {
            console.log(response);
            return undefined;
        }
    }).catch(function (error) {
        if (error.response.status == 403) {
            return 'Token expired';
        } else {
            console.log(error);
            return undefined;
        }
    });
    return result;
}

async function Delete(user, id) {
    var config = {
        headers: {
            'Content-type': 'application/json',
            'Authorization': user.auth,
            'X-CSRF-Token': user.csrf,
        }
    }
    var result = await axios.delete('http://dev.helpdesk.com/node/'+id+'?_format=json', config).then(function (response) {
        if (response.status == 200 || response.status == 204) {
            return true;
        } else {
            console.log(response);
            return undefined;
        }
    }).catch(function (error) {
        if (error.response.status == 403) {
            return 'Token expired';
        } else {
            console.log(error);
            return undefined;
        }
    });
    return result;
}

module.exports = {
    getAll: GetAll,
    get: Get,
    update: Update,
    delete: Delete,
}