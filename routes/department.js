var express = require('express');
var router = express.Router();

var userRepo = require('../repo/userRepo');
var taxonomyRepo = require('../repo/taxonomyRepo');

router.get('/', async function(req, res, next) {
    var user = userRepo.format(req.signedCookies);

    var departments = await taxonomyRepo.getAll(user, 'department');

    if(typeof departments == 'undefined') {
        res.render('error', {message: 'A problem has occured', error: {stack: 'accessing /authen/logout might solve the problem', status: ''}});
    } else if (departments == 'Token expired') {
        res.render('login', {error: 'Login expired'});
    } else {
        res.render('taxonomy_manage', { page: 'Department', taxonomies: departments });
    }
})

router.post('/delete', async function(req, res, next) {
    var user = userRepo.format(req.signedCookies);
    var result = await taxonomyRepo.delete(user, req.body.id);
    
    if(typeof result == 'undefined') {
        res.render('error', {message: 'A problem has occured', error: {stack: 'accessing /authen/logout might solve the problem', status: ''}});
    } else if (result == 'Token expired') {
        res.render('login', {error: 'Login expired'});
    } else {
        res.redirect('/department');
    }
})

router.post('/update/:id', async function(req, res, next) {
    var user = userRepo.format(req.signedCookies);
    var result = await taxonomyRepo.update(user, req.params.id, req.body, 'department');

    if(typeof result == 'undefined') {
        res.render('error', {message: 'A problem has occured', error: {stack: 'accessing /authen/logout might solve the problem', status: ''}});
    } else if (result == 'Token expired') {
        res.render('login', {error: 'Login expired'});
    } else {
        res.redirect('/department');
    }
})

module.exports = router;