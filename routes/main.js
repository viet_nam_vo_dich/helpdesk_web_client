var express = require('express');
var router = express.Router();

var userRepo = require('../repo/userRepo');
var mainRepo = require('../repo/mainRepo');

router.get('/', async function(req, res, next) {
    var user = userRepo.format(req.signedCookies);
    var results = await mainRepo.getDashboard(user);
    if(typeof results == 'undefined') {
        res.render('error', {message: 'A problem has occured', error: {stack: 'accessing /authen/logout might solve the problem', status: ''}});
    } else if (results == 'Token expired') {
        res.render('login', {error: 'Login expired'});
    } else {
        res.render('dashboard', {data: results});
    }
});

module.exports = router;