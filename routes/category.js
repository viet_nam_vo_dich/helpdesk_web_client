var express = require('express');
var router = express.Router();

var userRepo = require('../repo/userRepo');
var taxonomyRepo = require('../repo/taxonomyRepo');

router.get('/', async function(req, res, next) {
    var user = userRepo.format(req.signedCookies);

    var categories = await taxonomyRepo.getAll(user, 'complaint_category');

    if(typeof categories == 'undefined') {
        res.render('error', {message: 'A problem has occured', error: {stack: 'accessing /authen/logout might solve the problem', status: ''}});
    } else if (categories == 'Token expired') {
        res.render('login', {error: 'Login expired'});
    } else {
        res.render('taxonomy_manage', { page: 'Category', taxonomies: categories });
    }
})

router.post('/delete', async function(req, res, next) {
    var user = userRepo.format(req.signedCookies);
    var result = await taxonomyRepo.delete(user, req.body.id);
    
    if(typeof result == 'undefined') {
        res.render('error', {message: 'A problem has occured', error: {stack: 'accessing /authen/logout might solve the problem', status: ''}});
    } else if (result == 'Token expired') {
        res.render('login', {error: 'Login expired'});
    } else {
        res.redirect('/category');
    }
})

router.post('/update/:id', async function(req, res, next) {
    var user = userRepo.format(req.signedCookies);
    var result = await taxonomyRepo.update(user, req.params.id, req.body, 'complaint_category');

    if(typeof result == 'undefined') {
        res.render('error', {message: 'A problem has occured', error: {stack: 'accessing /authen/logout might solve the problem', status: ''}});
    } else if (result == 'Token expired') {
        res.render('login', {error: 'Login expired'});
    } else {
        res.redirect('/category');
    }
})

module.exports = router;