var axios = require('axios');
var dateFormat = require('dateformat');
var decode = require('decode-html');

async function GetAll(user) {
    var config = {
        headers: {
            'Content-type': 'application/json',
            'Authorization': user.auth,
        }
    }

    var result = await axios.get('http://dev.helpdesk.com/api/article?_format=json', config).then(function (response) {
        if (response.status == 200){
            response.data.forEach(function(val) {
                val.title = decode(val.title);
                val.content = decode(val.content);
                val.author = decode(val.author);
                val.created = dateFormat(val.created[0].value, 'yyyy-mm-dd HH:MM');
            });

            return response.data;
        } else {
            console.log(response);
            return undefined;
        }
    }).catch(function (error) {
        if(error.response.status == 403) {
            return 'Token expired';
        } else {
            console.log(error);
            return undefined;
        }
    });
    return result;
}

async function Update(user, id, article) {
    if(id == '0') {
        var url = 'http://dev.helpdesk.com/node?_format=json';
        var method = 'post';
    } else {
        var url = 'http://dev.helpdesk.com/node/'+id+'?_format=json';
        var method = 'patch';
    }

    var data = {
        type: [{target_id: 'article'}],
        title: [{value: article.title}],
        body: [{value: article.content}]
    }

    var result = await axios({
        method: method,
        url: url,
        data: data,
        headers: {
            'Content-type': 'application/json',
            'Authorization': user.auth,
            'X-CSRF-Token': user.csrf,
        }
    }).then(function (response) {
        if (response.status == 200 || response.status == 201) {
            return true;
        } else {
            console.log(response);
            return undefined;
        }
    }).catch(function (error) {
        if (error.response.status == 403) {
            return 'Token expired';
        } else {
            console.log(error);
            return undefined;
        }
    });
    return result;
}

async function Delete(user, id) {
    var config = {
        headers: {
            'Content-type': 'application/json',
            'Authorization': user.auth,
            'X-CSRF-Token': user.csrf,
        }
    }
    var result = await axios.delete('http://dev.helpdesk.com/node/'+id+'?_format=json', config).then(function (response) {
        if (response.status == 200 || response.status == 204) {
            return true;
        } else {
            console.log(response);
            return undefined;
        }
    }).catch(function (error) {
        if (error.response.status == 403) {
            return 'Token expired';
        } else {
            console.log(error);
            return undefined;
        }
    });
    return result;
}

module.exports = {
    getAll: GetAll,
    update: Update,
    delete: Delete,
}