var express = require('express');
var router = express.Router();

var complaintRepo = require('../repo/complaintRepo');
var userRepo = require('../repo/userRepo');
var taxonomyRepo = require('../repo/taxonomyRepo');

router.get('/', async function(req, res, next) {
    var user = userRepo.format(req.signedCookies);
    var complaints = await complaintRepo.getAll(user);
    if(typeof complaints == 'undefined') {
        res.render('error', {message: 'A problem has occured', error: {stack: 'accessing /authen/logout might solve the problem', status: ''}});
    } else if (complaints == 'Token expired') {
        res.render('login', {error: 'Login expired'});
    } else {
        res.render('complaint_manage', {complaints: complaints});
    }
})

router.post('/', function(req, res, next) {
    res.send('/complaint/filter');
})

router.post('/delete', async function(req, res, next) {
    var user = userRepo.format(req.signedCookies);
    var result = await complaintRepo.delete(user, req.body.id);
    
    if(typeof result == 'undefined') {
        res.render('error', {message: 'A problem has occured', error: {stack: 'accessing /authen/logout might solve the problem', status: ''}});
    } else if (result == 'Token expired') {
        res.render('login', {error: 'Login expired'});
    } else {
        res.redirect('/complaint');
    }
})

router.get('/add', function(req, res, next) {
    var user = userRepo.format(req.signedCookies);

    Promise.all(
        [taxonomyRepo.getAll(user, 'complaint_category'),
        taxonomyRepo.getAll(user, 'department'), 
        userRepo.getAll(user, {roles: ['technician']})
    ]).then(function(value){
        var categories = value[0];
        var departments = value[1];
        var technicians = value[2];

        if(typeof technicians == 'undefined' || typeof departments == 'undefined' || typeof categories == 'undefined') {
            res.render('error', {message: 'A problem has occured', error: {stack: 'accessing /authen/logout might solve the problem', status: ''}});
        } else if (technicians == 'Token expired' || departments == 'Token expired' || categories == 'Token expired') {
            res.render('login', {error: 'Login expired'});
        } else {
            res.render('complaint_form', { categories: categories, departments: departments, technicians: technicians });
        }
    }).catch(function(e){
        console.log(e);
        res.render('error', {message: 'A problem has occured', error: {stack: 'accessing /authen/logout might solve the problem', status: ''}});
    });
})

router.get('/update/:id', function(req, res, next) {
    var user = userRepo.format(req.signedCookies);

    Promise.all(
        [complaintRepo.get(user, req.params.id),
        taxonomyRepo.getAll(user, 'complaint_category'),
        taxonomyRepo.getAll(user, 'department'), 
        userRepo.getAll(user, {roles: ['technician']})
    ]).then(function(value){
        var complaint = value[0];
        var categories = value[1];
        var departments = value[2];
        var technicians = value[3];

        if(typeof complaint == 'undefined' || typeof technicians == 'undefined' || typeof departments == 'undefined' || typeof categories == 'undefined') {
            res.render('error', {message: 'A problem has occured', error: {stack: 'accessing /authen/logout might solve the problem', status: ''}});
        } else if (complaint == 'Token expired' || technicians == 'Token expired' || departments == 'Token expired' || categories == 'Token expired') {
            res.render('login', {error: 'Login expired'});
        } else {
            res.render('complaint_form', 
            {
                complaint: complaint,
                categories: categories,
                departments: departments,
                technicians: technicians,
            });
        }
    }).catch(function(e){
        console.log(e);
        res.render('error', {message: 'A problem has occured', error: {stack: 'accessing /authen/logout might solve the problem', status: ''}});
    });
})

router.post('/update/:id', async function(req, res, next) {
    var user = userRepo.format(req.signedCookies);
    var result = await complaintRepo.update(user, req.params.id, req.body);
    
    if(typeof result == 'undefined') {
        res.render('error', {message: 'A problem has occured', error: {stack: 'accessing /authen/logout might solve the problem', status: ''}});
    } else if (result == 'Token expired') {
        res.render('login', {error: 'Login expired'});
    } else {
        res.redirect('/complaint');
    }
})

module.exports = router;