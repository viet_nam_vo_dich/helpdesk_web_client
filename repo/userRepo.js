var axios = require('axios');

async function Login(username, password) {
    var data = {
        name: username,
        pass: password,
    }
    var config = {
        headers: {
            'Content-type': 'application/json'
        }
    }
    var result = await axios.post('http://dev.helpdesk.com/user/login?_format=json', data, config).then(function (response) {
        if (response.status == 200 ){
            return response.data;
        } else {
            console.log(response);
            return undefined;
        }
    }).catch(function (error) {
        console.log(error);
        return undefined;
    });
    return result;
}

function FormatUser(cookie) {
    return {
        auth: 'Bearer ' + cookie.user_auth,
        csrf: cookie.csrf_token,
        id: cookie.user_id,
    }
}

async function GetAll(user, filter) {
    var config = {
        headers: {
            'Content-type': 'application/json',
            'Authorization': user.auth,
        }
    }

    var url = 'http://dev.helpdesk.com/api/users?_format=json';

    if(typeof filter != 'undefined'){
        typeof filter.roles != 'undefined'  ? filter.roles.forEach(function(val){ url+='&roles[]='+val }) : url+='';
        typeof filter.name != 'undefined' ? url += '&name='+filter.name : url+='';
    }

    var result = await axios.get(url, config).then(function (response) {
        if (response.status == 200){
            return response.data;
        } else {
            console.log(response);
            return undefined;
        }
    }).catch(function (error) {
        if(error.response.status == 403) {
            return 'Token expired';
        } else {
            console.log(error);
            return undefined;
        }
    });
    return result;
}

async function Get(user, id) {
    if(id == 1) {
        return undefined
    }

    var config = {
        headers: {
            'Content-type': 'application/json',
            'Authorization': user.auth,
        }
    }

    var result = await axios.get('http://dev.helpdesk.com/user/'+id+'?_format=json', config).then(function (response) {
        if (response.status == 200){
            var data = response.data;

            var roles = [];
            data.roles.forEach(function (val) {
                roles.push(val.target_id);
            });

            var user = {
                id: data.uid[0].value,
                name: data.name[0].value,
                mail: data.mail[0].value,
                status: data.status[0].value,
                roles: roles,
                department: (data.field_department.length > 0 ? data.field_department[0].target_id : ''),
                occupation: (data.field_occupation.length > 0 ? data.field_occupation[0].value : ''),
                phone: data.field_phone[0].value,
            }

            return user;
        } else {
            console.log(response);
            return undefined;
        }
    }).catch(function (error) {
        if(error.response.status == 403) {
            console.log(error.response);
            return {
                status: 403,
                message: 'Token expired',
            };
        } else {
            console.log(error);
            return undefined;
        }
    });
    return result;
}

async function Update(user, id, data_user) {
    if(id == 1) {
        return undefined
    }

    if(id == '0') {
        var url = 'http://dev.helpdesk.com/entity/user?_format=json';
        var method = 'post';
    } else {
        var url = 'http://dev.helpdesk.com/user/'+id+'?_format=json';
        var method = 'patch';
    }

    var roles = [];
    if(Array.isArray(data_user.roles)){
        data_user.roles.forEach(function(val){
            if(val != 0) {
                roles.push(val);
            }
        });
    } else {
        if(data_user.roles != 0) {
            roles.push(data_user.roles);
        }
    }

    var data = {
        name: [{value: data_user.name}],
        mail: [{value: data_user.mail}],
        field_phone: [{value: data_user.phone}],
        field_department: (data_user.department != 0 ? [{target_id: complaint.department}] : []),
        field_occupation: (data_user.occupation != 0 ? [{value: data_user.occupation}] : []),
        roles: roles,
        status: [{value: (data_user.status == '0' ? false : true)}],
    }

    if(data_user.pass != '') {
        data.pass = [{value: data_user.pass}];
    }

    var result = await axios({
        method: method,
        url: url,
        data: data,
        headers: {
            'Content-type': 'application/json',
            'Authorization': user.auth,
            'X-CSRF-Token': user.csrf,
        }
    }).then(function (response) {
        if (response.status == 200 || response.status == 201) {
            return true;
        } else {
            console.log(response);
            return undefined;
        }
    }).catch(function (error) {
        if (error.response.status == 403) {
            return 'Token expired';
        } else if(error.response.status == 422) {
            return {
                status: 422,
                message: error.response.data.message,
            }
        } else {
            console.log(error);
            return undefined;
        }
    });
    return result;
}

async function Ban(user, id, status){
    if(id == 1) {
        return undefined
    }
    
    var config = {
        headers: {
            'Content-type': 'application/json',
            'Authorization': user.auth,
            'X-CSRF-Token': user.csrf,
        }
    }

    var data = {
        status: [{value: (status == 'Yes' ? false : true)}]
    }

    var result = await axios.patch('http://dev.helpdesk.com/user/'+id+'?_format=json', data , config).then(function (response) {
        if (response.status == 200){
            return true;
        } else {
            console.log(response);
            return undefined;
        }
    }).catch(function (error) {
        if(error.response.status == 403) {
            console.log(error.response);
            return {
                status: 403,
                message: 'Token expired',
            };
        } else {
            console.log(error);
            return undefined;
        }
    });
    return result;
}

module.exports = {
    login: Login,
    format: FormatUser,
    getAll: GetAll,
    get: Get,
    update: Update,
    ban: Ban,
}