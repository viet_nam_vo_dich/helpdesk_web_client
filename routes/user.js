var express = require('express');
var router = express.Router();

var userRepo = require('../repo/userRepo');
var taxonomyRepo = require('../repo/taxonomyRepo');

router.get('/', async function(req, res, next) {
    var user = userRepo.format(req.signedCookies);
    var users = await userRepo.getAll(user);

    if(typeof users == 'undefined') {
        res.render('error', {message: 'A problem has occured', error: {stack: 'accessing /authen/logout might solve the problem', status: ''}});
    } else if (users == 'Token expired') {
        res.render('login', {error: 'Login expired'});
    } else {
        res.render('user_manage', { users: users});
    }
})

router.post('/', function(req, res, next) {
    res.send('/user/filter');
})

router.post('/ban', async function(req, res, next) {
    var user = userRepo.format(req.signedCookies);
    var result = await userRepo.ban(user, req.body.id, req.body.status);
    
    if(typeof result == 'undefined') {
        res.render('error', {message: 'A problem has occured', error: {stack: 'accessing /authen/logout might solve the problem', status: ''}});
    } else if (result == 'Token expired') {
        res.render('login', {error: 'Login expired'});
    } else {
        res.redirect('/user');
    }
})

router.get('/add', async function(req, res, next) {
    var user = userRepo.format(req.signedCookies);

    var departments = await taxonomyRepo.getAll(user, 'department');

    if(typeof departments == 'undefined') {
        res.render('error', {message: 'A problem has occured', error: {stack: 'accessing /authen/logout might solve the problem', status: ''}});
    } else if (departments == 'Token expired') {
        res.render('login', {error: 'Login expired'});
    } else {
        res.render('user_form', { departments: departments });
    }
})

router.get('/update/:id', function(req, res, next) {
    var user = userRepo.format(req.signedCookies);

    Promise.all(
        [userRepo.get(user, req.params.id),
        taxonomyRepo.getAll(user, 'department')
    ]).then(function(value){
        var user = value[0];
        var departments = value[1];

        if(typeof user == 'undefined' || typeof departments == 'undefined') {
            res.render('error', {message: 'A problem has occured', error: {stack: 'accessing /authen/logout might solve the problem', status: ''}});
        } else if (user == 'Token expired' || departments == 'Token expired') {
            res.render('login', {error: 'Login expired'});
        } else {
            res.render('user_form', 
            {
                user: user,
                departments: departments,
            });
        }
    }).catch(function(e){
        console.log(e);
        res.render('error', {message: 'A problem has occured', error: {stack: 'accessing /authen/logout might solve the problem', status: ''}});
    });
})

router.post('/update/:id', async function(req, res, next) {
    var user = userRepo.format(req.signedCookies);
    var result = await userRepo.update(user, req.params.id, req.body);
    
    if(typeof result == 'undefined') {
        res.render('error', {message: 'A problem has occured', error: {stack: 'accessing /authen/logout might solve the problem', status: ''}});
    } else if (result == 'Token expired') {
        res.render('login', {error: 'Login expired'});
    } else if (result.status == 422) {
        res.render('error', {message: result.message, error: {status: '<a href="/user/update/'+req.params.id+'">Back</a>', stack: ''}});
    } else {
        res.redirect('/user');
    }
})

module.exports = router;