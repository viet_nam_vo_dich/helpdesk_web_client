var axios = require('axios');

async function GetDashboard(user) {
    var config = {
        headers: {
            'Content-type': 'application/json',
            'Authorization': user.auth,
        }
    }
    var result = await axios.get('http://dev.helpdesk.com/api/client/dashboard', config).then(function (response) {
        if (response.status == 200) {
            return response.data;
        } else {
            console.log(response);
            return undefined;
        }
    }).catch(function (error) {
        if (error.response.status == 403) {
            return 'Token expired';
        } else {
            console.log(error);
            return undefined;
        }
    });
    return result;
}

module.exports = {
    getDashboard: GetDashboard,
}