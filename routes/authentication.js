var express = require('express');
var router = express.Router();

var userRepo = require('../repo/userRepo');

router.get('/login', function(req, res, next) {
  res.render('login');
});

router.post('/login', async function(req, res, next) {
  var result = await userRepo.login(req.body.username, req.body.password);
  if(typeof result != 'undefined' && typeof result.current_user.roles != 'undefined' && result.current_user.roles.indexOf('moderator') != -1) {
    Promise.all(
      [res.cookie('user_auth', result.access_token, { signed : true, maxAge: 30*60*1000}),
      res.cookie('csrf_token', result.csrf_token, {signed: true, maxAge: 30*60*1000}),
      res.cookie('user_id', result.current_user.uid, {signed: true, maxAge: 30*60*1000})]
    );
    res.redirect('/')
  } else {
    res.render('login', { error: 'Cannot authenticate' });
  }
});

router.get('/logout', function(req, res, next) {
  Promise.all(
    [res.clearCookie('user_auth'),
    res.clearCookie('csrf_token'),
    res.clearCookie('user_id')]
  );
  res.redirect('/authen/login');
});

module.exports = router;
