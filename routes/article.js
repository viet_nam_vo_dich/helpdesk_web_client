var express = require('express');
var router = express.Router();

var userRepo = require('../repo/userRepo');
var articleRepo = require('../repo/articleRepo');

router.get('/', async function(req, res, next) {
    var user = userRepo.format(req.signedCookies);

    var articles = await articleRepo.getAll(user);

    if(typeof articles == 'undefined') {
        res.render('error', {message: 'A problem has occured', error: {stack: 'accessing /authen/logout might solve the problem', status: ''}});
    } else if (articles == 'Token expired') {
        res.render('login', {error: 'Login expired'});
    } else {
        res.render('article_manage', { articles: articles });
    }
})

router.post('/delete', async function(req, res, next) {
    var user = userRepo.format(req.signedCookies);
    var result = await articleRepo.delete(user, req.body.id);
    
    if(typeof result == 'undefined') {
        res.render('error', {message: 'A problem has occured', error: {stack: 'accessing /authen/logout might solve the problem', status: ''}});
    } else if (result == 'Token expired') {
        res.render('login', {error: 'Login expired'});
    } else {
        res.redirect('/article');
    }
})

router.post('/update/:id', async function(req, res, next) {
    var user = userRepo.format(req.signedCookies);
    var result = await articleRepo.update(user, req.params.id, req.body);

    if(typeof result == 'undefined') {
        res.render('error', {message: 'A problem has occured', error: {stack: 'accessing /authen/logout might solve the problem', status: ''}});
    } else if (result == 'Token expired') {
        res.render('login', {error: 'Login expired'});
    } else {
        res.redirect('/article');
    }
})

module.exports = router;