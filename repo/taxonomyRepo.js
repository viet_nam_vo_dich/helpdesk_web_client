var axios = require('axios');
var decode = require('decode-html');

async function GetAll(user, vocab) {

    var url = '';
    switch (vocab) {
        case 'department':
            url = 'http://dev.helpdesk.com/api/department?_format=json';
            break;
        case 'complaint_category':
            url = 'http://dev.helpdesk.com/api/complaint/category?_format=json';
            break;
    }

    var config = {
        headers: {
            'Content-type': 'application/json',
            'Authorization': user.auth,
        }
    }

    var result = await axios.get(url, config).then(function (response) {
        if (response.status == 200){
            response.data.forEach(function(val) {
                val.name = decode(val.name);
            });

            return response.data;
        } else {
            console.log(response);
            return undefined;
        }
    }).catch(function (error) {
        if(error.response.status == 403) {
            return 'Token expired';
        } else {
            console.log(error);
            return undefined;
        }
    });
    return result;
}

async function Update(user, id, taxonomy, type) {
    if(id == '0') {
        var url = 'http://dev.helpdesk.com/taxonomy/term?_format=json';
        var method = 'post';
    } else {
        var url = 'http://dev.helpdesk.com/taxonomy/term/'+id+'?_format=json';
        var method = 'patch';
    }

    var data = {
        vid: [{target_id: type}],
        name: [{value: taxonomy.name}],
    }

    var result = await axios({
        method: method,
        url: url,
        data: data,
        headers: {
            'Content-type': 'application/json',
            'Authorization': user.auth,
            'X-CSRF-Token': user.csrf,
        }
    }).then(function (response) {
        if (response.status == 200 || response.status == 201) {
            return true;
        } else {
            console.log(response);
            return undefined;
        }
    }).catch(function (error) {
        if (error.response.status == 403) {
            return 'Token expired';
        } else {
            console.log(error);
            return undefined;
        }
    });
    return result;
}

async function Delete(user, id) {
    var config = {
        headers: {
            'Content-type': 'application/json',
            'Authorization': user.auth,
            'X-CSRF-Token': user.csrf,
        }
    }
    var result = await axios.delete('http://dev.helpdesk.com/taxonomy/term/'+id+'?_format=json', config).then(function (response) {
        if (response.status == 200 || response.status == 204) {
            return true;
        } else {
            console.log(response);
            return undefined;
        }
    }).catch(function (error) {
        if (error.response.status == 403) {
            return 'Token expired';
        } else {
            console.log(error);
            return undefined;
        }
    });
    return result;
}

module.exports = {
    getAll: GetAll,
    update: Update,
    delete: Delete,
}