var createError = require('http-errors');
var express = require('express');
var path = require('path');
var cookieParser = require('cookie-parser');
var logger = require('morgan');
var axios = require('axios');

var authenticationRouter = require('./routes/authentication');
var mainRouter = require('./routes/main');
var complaintRouter = require('./routes/complaint');
var userRouter = require('./routes/user');
var categoryRouter = require('./routes/category');
var departmentRouter = require('./routes/department');
var articleRouter = require('./routes/article');

var app = express();

// view engine setup
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'ejs');

app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use(cookieParser('dev.helpdesk.com'));
app.use(express.static(path.join(__dirname, 'public')));

// authentication related routes
app.use('/authen', authenticationRouter);

// authentication check
app.use(function(req, res, next) {
  if (typeof req.signedCookies.user_auth == 'undefined' || typeof req.signedCookies.csrf_token == 'undefined' || typeof req.signedCookies.user_id == 'undefined') {
    res.redirect('/authen/login');
  } else {
    res.locals.user_id =  req.signedCookies.user_id;
  }
  next();
});

// main routes
app.use('/', mainRouter);

app.use('/complaint', complaintRouter);

app.use('/user', userRouter);

app.use('/category', categoryRouter);

app.use('/department', departmentRouter);

app.use('/article', articleRouter);

// catch 404 and forward to error handler
app.use(function(req, res, next) {
  next(createError(404));
});

// error handler
app.use(function(err, req, res, next) {
  // set locals, only providing error in development
  res.locals.message = err.message;
  res.locals.error = req.app.get('env') === 'development' ? err : {};

  // render the error page
  res.status(err.status || 500);
  res.render('error');
});

app.listen(3000);

module.exports = app;
